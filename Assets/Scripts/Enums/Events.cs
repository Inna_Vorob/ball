﻿namespace Enums
{
    public enum Events
    {
        ShowMainMenu,
        StartGame,
        GameEnd
    }
}