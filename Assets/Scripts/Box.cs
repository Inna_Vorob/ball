﻿using UnityEngine;
using UnityEngine.UI;

public class Box : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private RectTransform _transform;

    public RectTransform Transform { get { return _transform; } }

    public void SetColor(Color color)
    {
        _image.color = color;
    }

    public void SetSize(Vector2 size)
    {
        _transform.sizeDelta = size;
    }

    public void SetPosition(Vector3 pos)
    {
        _transform.localPosition = pos;
    }

    public bool IsActive()
    {
        return gameObject.activeInHierarchy;
    }

    public void SetActive(bool set)
    {
        gameObject.SetActive(set);
    }

}
