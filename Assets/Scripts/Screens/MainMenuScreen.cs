﻿using Enums;
using Controllers;

namespace Screens
{
    public class MainMenuScreen : BaseScreen
    {
        public override GameScreens Screen { get { return GameScreens.MainMenu; } }

        public void ButtonStartHandler()
        {
            EventController.Instance.TriggerEvent(Events.StartGame);
        }
    }
} 
