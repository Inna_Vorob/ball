﻿using Enums;

namespace Screens
{
    public class GameEndScreen : BaseScreen
    {
        public override GameScreens Screen { get { return GameScreens.GameEnd; } }

        public void ButtonStartHandler()
        {
            Controllers.EventController.Instance.TriggerEvent(Events.StartGame);
        }
    }
}
