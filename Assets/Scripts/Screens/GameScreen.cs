﻿using Enums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Screens
{
    public class GameScreen : BaseScreen, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [Space(10)]
        [Header("Health")]
        [SerializeField] private List<GameObject> _healths;

        [Space(10)]
        [Header("Platform")]
        [SerializeField] private RectTransform _platform;

        [Space(10)]
        [Header("Ball")]
        [SerializeField] private RectTransform _ball;
        [SerializeField] private Vector2 _direction = new Vector2(-0.75f, 0.5f);
        [Range(1, 500)]
        [SerializeField] private int _speed = 450;

        [Space(10)]
        [Header("Box")]
        [SerializeField] private GameObject _baseBox;

        public override GameScreens Screen { get { return GameScreens.Game; } }

        private Vector3 _scaleStep = new Vector3(0, 0.5f, 0);
        private float _rightSide;
        private float _leftSide;
        private float _topSide;
        private float _bottomSide;

        private Vector3 _startPositionBall;
        private Vector3 _startPositionPlatform;

        private IEnumerator _ballAnimation = null;

        private const int _standartWidth = 1080;
        private const int _standartHeight = 1920;

        public override void Initialize()
        {
            _rightSide = UnityEngine.Screen.width / 2f;
            _leftSide = -_rightSide;
            _topSide = UnityEngine.Screen.height / 2f;
            _bottomSide = -_topSide;
            _startPositionPlatform = _platform.position;
            _startPositionBall = _ball.position;
            SetObjectSize();
            SetSpeed();
        }       

        public override void Show()
        {
            base.Show();

            foreach (var item in _healths)
                item.SetActive(true);

            _platform.position = _startPositionPlatform;
            _ball.position = _startPositionBall; 
             
            _ballAnimation = MoveBall();
            StartCoroutine(_ballAnimation);
        }

        public override void Hide()
        {
            StopMoveBall();
            base.Hide();             
        }

        public void OnDrag(PointerEventData eventData)
        {
            float x = _platform.localPosition.x + eventData.delta.x;
            float check = Mathf.Clamp(x, _leftSide + _platform.sizeDelta.x / 2f, _rightSide - _platform.sizeDelta.x / 2f);

            _platform.localPosition = new Vector3(check, _platform.localPosition.y, _platform.localPosition.z);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _platform.localScale += _scaleStep;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _platform.localScale -= _scaleStep;
        }

        #region Private_Methods

        private IEnumerator MoveBall()
        {             
            while (true)
            {
                if (_ball.localPosition.y > _bottomSide)
                {
                    _ball.localPosition = new Vector3(_ball.localPosition.x + _direction.x * _speed * Time.deltaTime,
                    _ball.localPosition.y + _direction.y * _speed * Time.deltaTime, _ball.localPosition.z); 
                }

                CheckWallsCollision();
                CheckDistanceToPlatform();
                CheckDistanceToBoxes();
                yield return new WaitForFixedUpdate();
            }
        }

        private void StopMoveBall()
        {
            if (_ballAnimation != null)
            {
                StopCoroutine(_ballAnimation);
                _ballAnimation = null;
            }
        }

        private void CheckWallsCollision()
        {
            if (_ball.localPosition.x < _leftSide + _ball.sizeDelta.x / 2f ||
                _ball.localPosition.x > _rightSide - _ball.sizeDelta.x / 2f)
                _direction.x *= -1;

            if (_ball.localPosition.y > _topSide - _ball.sizeDelta.y / 2f)
                _direction.y *= -1;

            if (_ball.localPosition.y <= _bottomSide + _ball.sizeDelta.y / 2f)
                UpdateHealth();
        }     

        private void UpdateHealth()
        {
            if (Controllers.HealthController.Health > 0)
            {
                Controllers.HealthController.Health--;
                _healths[Controllers.HealthController.Health].SetActive(false);
                _ball.position = _startPositionBall;
            }
            else
            {
                StopMoveBall();
                Controllers.EventController.Instance.TriggerEvent(Events.GameEnd);
            }                 
        }

        private void CheckDistanceToPlatform()
        {
            Vector2 heading =  _ball.position - 
                new Vector3(_platform.position.x, _platform.position.y + _platform.sizeDelta.y/2f + _ball.sizeDelta.y/2f, _platform.position.z);
            var distance = heading.magnitude;
            Vector2 direction = heading / distance;

            if (direction.y <= 0f && _ball.position.y > _platform.position.y + _platform.sizeDelta.y / 2f)
            {
                var checkStart = _platform.position.x - _platform.sizeDelta.x / 2f - _ball.sizeDelta.x / 2f;
                var checkEnd = _platform.position.x + _platform.sizeDelta.x / 2f + _ball.sizeDelta.x / 2f;

                if (_ball.position.x >= checkStart && _ball.position.x < checkEnd ||
                    _ball.position.x > checkStart && _ball.position.x <= checkEnd)
                    _direction.y *= -1;
            }
        }

        private void CheckDistanceToBoxes()
        {
            if (Controllers.BoxController.Instance.GetCountOfActiveBoxes() == 0)
            {
                StopMoveBall();
                Controllers.EventController.Instance.TriggerEvent(Events.GameEnd);
            }
            else
            {
                foreach (var item in Controllers.BoxController.Instance.PoolBoxes)
                {
                    if (item.IsActive() && IsBallCloseToBox(item.Transform))
                    {
                        _direction.y *= -1;
                        item.SetActive(false);
                    }
                }
            }           
        }

        private bool IsBallCloseToBox(RectTransform box)
        {
            Vector2 heading = _ball.position - 
                new Vector3(box.position.x, box.position.y - box.sizeDelta.y / 2 - _ball.sizeDelta.y / 2, box.position.z);

            var distance = heading.magnitude;
            Vector2 direction = heading / distance;

            if (direction.y >= 0)
            {
                var checkStart = box.position.x - box.sizeDelta.x / 2f - _ball.sizeDelta.x / 2f;
                var checkEnd = box.position.x + box.sizeDelta.x / 2f + _ball.sizeDelta.x / 2f;

                if (_ball.position.x >= checkStart && _ball.position.x < checkEnd ||
                    _ball.position.x > checkStart && _ball.position.x <= checkEnd)
                    return true;
            }
            return false;
        }

        private void SetObjectSize()
        {
            _ball.sizeDelta = new Vector2((UnityEngine.Screen.height * _ball.sizeDelta.y) / _standartHeight,
               (UnityEngine.Screen.height * _ball.sizeDelta.y) / _standartHeight);
            _platform.sizeDelta = new Vector2((UnityEngine.Screen.width * _platform.sizeDelta.x) / _standartWidth,
               (UnityEngine.Screen.height * _platform.sizeDelta.y) / _standartHeight);

            foreach (var item in _healths)
            {
                var heart = item.GetComponent<RectTransform>();
                heart.sizeDelta = new Vector2((UnityEngine.Screen.height * heart.sizeDelta.y) / _standartHeight,
                (UnityEngine.Screen.height * heart.sizeDelta.y) / _standartHeight);
            }
        }

        private void SetSpeed()
        {
            _speed = _speed * UnityEngine.Screen.height / UnityEngine.Screen.width;        
        }

        #endregion
    }
}
