﻿using UnityEngine;
using Enums;

namespace Screens
{
    public class BaseScreen : MonoBehaviour
    {
        [SerializeField] protected GameObject _content;
        public virtual GameScreens Screen { get { return GameScreens.none; } }

        public virtual void Initialize()
        {

        }

        public virtual void Hide()
        {
            _content.SetActive(false);
        }

        public virtual void Show()
        {
            _content.SetActive(true);
        }
    } 
}
 
