﻿using UnityEngine;

namespace Controllers
{
    public class HealthController : MonoBehaviour
    {
        private static int _health;
        public static int Health {
            get
            {
                return _health;
            }
            set
            {
                if (value < 0)
                    _health = 0;
                else
                    _health = value;
            }
        }
    }
}
 

