﻿using UnityEngine.Events;
using System.Collections.Generic;
using Enums;

namespace Controllers
{
    public class EventController 
    {
        #region Singleton

        private static EventController _instance;
        public static EventController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new EventController();
                return _instance;
            }
        }

        private EventController() { }

        #endregion

        private Dictionary<Events, UnityEvent> _eventDictionary = new Dictionary<Events, UnityEvent>();

        public void StartListening(Events eventName, UnityAction listener)
        {
            UnityEvent thisEvent = null;
            if (_eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEvent();
                thisEvent.AddListener(listener);
                _eventDictionary.Add(eventName, thisEvent);
            }
        }

        public void StopListening(Events eventName, UnityAction listener)
        {
            UnityEvent thisEvent = null;
            if (_eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public void TriggerEvent(Events eventName)
        {
            UnityEvent thisEvent = null;
            if (_eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke();
            }
        }
    }
}
 
