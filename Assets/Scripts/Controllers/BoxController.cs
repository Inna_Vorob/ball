﻿using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace Controllers
{
    public class BoxController : SingletonMonobehaviour<BoxController>
    {
        [SerializeField] private GameObject _baseBox;

        [Space(10)]
        [Header("Grid Settings")]
        [SerializeField] private int _gridSpaceX = 100;
        [SerializeField] private int _gridSpaceY = 50;
        [Range(0,0.95f)]
        [SerializeField] private float _positionOnScreen = 0.78f;
        [Range(10, 30)]
        [SerializeField] private float _cellSizeY = 25f;

        private List<Box> _poolBoxes = new List<Box>();
        public List<Box> PoolBoxes { get { return _poolBoxes; } }

        private float _rightSide;
        private float _leftSide;
        private float _topSide;

        public void Initialize()
        {
            _rightSide = Screen.width / 2f;
            _leftSide = -_rightSide;
            _topSide = Screen.height / 2f;           
        }

        public void SpawnBoxes(int lines, int countPerLine)
        {
            Box box;

            var emptySpace = Screen.width - _gridSpaceX;
            var step = (Screen.width - emptySpace) / (countPerLine + 1);

            Vector2 boxSize = new Vector2(emptySpace / countPerLine, Screen.height / _cellSizeY);
            Color boxColor = GetRandomColor();

            float x = _leftSide + step + boxSize.x / 2f;
            float y = _topSide * _positionOnScreen;

            for (int i = 0; i < lines; i++)
            {
                for (int j = 0; j < countPerLine; j++)
                {
                    box = GetPoolBox();
                    box.SetPosition(new Vector2(x, y));
                    box.SetSize(boxSize);
                    box.SetColor(boxColor);

                    x += step + boxSize.x;
                }

                x = _leftSide + step + boxSize.x / 2f;
                y -= boxSize.y + _gridSpaceY;
                boxColor = GetRandomColor(); 
            }             
        }

        public void DiactivateBoxes()
        {
            foreach (var item in _poolBoxes)
                item.SetActive(false);
        }

        public int GetCountOfActiveBoxes()
        {
            int res = 0;

            foreach (var item in _poolBoxes)
            {
                if (item.IsActive())
                    res++;
            }

            return res;
        }

        #region Private_Methods

        private Box GetPoolBox()
        {
            Box box = null;

            for (int i = 0; i < _poolBoxes.Count; i++)
            {
                if (!_poolBoxes[i].IsActive())
                {
                    _poolBoxes[i].SetActive(true);
                    box = _poolBoxes[i];
                    break;
                }
            }

            if (box == null)
            {
                Box boxObject = Instantiate(_baseBox, transform).GetComponent<Box>();
                box = boxObject;
                _poolBoxes.Add(boxObject);
            }

            return box;
        }

        private Color GetRandomColor()
        {
            return new Color(Random.Range(0.15F, 1F), Random.Range(0.15F, 1F), Random.Range(0.15F, 1F));
        }

        #endregion
    }
}

