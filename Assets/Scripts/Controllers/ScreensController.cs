﻿using System.Collections.Generic;
using Enums;
using Screens;
using UnityEngine;

namespace Controllers
{
    public class ScreensController : MonoBehaviour
    {
        [SerializeField] private MainMenuScreen _mainMenuScreen;
        [SerializeField] private GameScreen _gameScreen;
        [SerializeField] private GameEndScreen _gameEndScreen;

        private Dictionary<GameScreens, BaseScreen> _screens;
        private Dictionary<GameScreens, BaseScreen> Screens
        {
            get
            {
                if (_screens == null)
                {
                    _screens = new Dictionary<GameScreens, BaseScreen>();
                    SetScreens();
                    InitializeScreens();
                }
                return _screens;
            }
        }

        private GameScreens _currentScreen = GameScreens.none;

        public void ShowMainMenu()
        {
            ShowScreen(GameScreens.MainMenu);
        }

        public void ShowGame()
        {
            ShowScreen(GameScreens.Game);
        }

        public void ShowGameEnd()
        {
            ShowScreen(GameScreens.GameEnd);
        }

        #region PrivateMethods

        private void SetScreens()
        {
            _screens.Add(_mainMenuScreen.Screen, _mainMenuScreen);
            _screens.Add(_gameScreen.Screen, _gameScreen);
            _screens.Add(_gameEndScreen.Screen, _gameEndScreen);
        }

        private void InitializeScreens()
        {
            foreach (var item in _screens)
            {
                item.Value.Initialize();
            }
        }

        private void ShowScreen(GameScreens screen)
        {
            if (Screens.ContainsKey(screen))
            {
                if (Screens.ContainsKey(_currentScreen))
                    Screens[_currentScreen].Hide();

                _currentScreen = screen;

                if (Screens.ContainsKey(_currentScreen))
                    Screens[_currentScreen].Show();
            }

        }

        #endregion
    }

}
