﻿using Enums;
using UnityEngine;

namespace Controllers
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private ScreensController _screenController;
        [SerializeField] private BoxController _boxController;

        private void Start()
        { 
            AddListeners();
            _boxController.Initialize();
            _screenController.ShowMainMenu();
        }

        private void AddListeners()
        {
            EventController.Instance.StartListening(Events.StartGame, StartGame);
            EventController.Instance.StartListening(Events.GameEnd, EndGame);
        }

        private void Init()
        {
            _screenController.ShowGame();             
        }

        private void StartGame()
        {
            HealthController.Health = 3;
            _boxController.SpawnBoxes(5,4);
            _screenController.ShowGame();
        }

        private void EndGame()
        {
            _screenController.ShowGameEnd();
            _boxController.DiactivateBoxes();
        }
    }
}
